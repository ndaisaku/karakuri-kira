// Import the interfaces
#import "MiddleScene.h"
#import "ScenesManager.h"
#import "SoundPlayer.h"

#define PTM_RATIO 32

// enums that will be used as tags
enum {
	kTagTileMap = 1,
	kTagBatchNode = 1,
	kTagAnimation1 = 1,
};


@implementation MiddleScene

+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
	CCScene *scene = [MiddleScene node];
	CCLayer *layer = [MiddleLayer layerWithSceneId:sceneId:nextPage];
	
	// 作成したMiddleSceneクラスのレイヤーをsceneに追加した上で
    // sceneの方を返しています。
	[scene addChild: layer];
    
	return scene;
}

@end


#pragma mark -

@implementation MiddleLayer
#pragma mark インスタンスの初期化/解放
// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
    return [[[self alloc] initWithSceneId:sceneId:nextPage] autorelease];
}

- (id)initWithSceneId:(NSInteger)sceneId:(BOOL)nextPage{
	
	if( (self=[super init])) {
        
        page = sceneId-2;
        next = nextPage;
        CCLOG(@"page%d",page);
       
        //設定を読み込み
        [self setteiRead];
        setteino[0] = optionnum3[0];
        setteino[1] = optionnum3[1];
        setteino[2] = optionnum3[2];
        
        //BGMの読み込み
        if(page == 1 && next){
            [SoundPlayer bgmLoad:optionnum3[1]];
            [SoundPlayer sePlay:optionnum3[1]:page];
        }
        
        //タッチの判定
        touche =false;
        
        //石カウント
        ishitouch =0;
        
        //鐘の音制御
        kane = false;
        kanecount =0;
        
        
        
        
		//画面サイズ
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
		// enable touches
		self.isTouchEnabled = YES;
		
		// enable accelerometer
		self.isAccelerometerEnabled = NO;
		
		CGSize screenSize = [CCDirector sharedDirector].winSize;
		
		// Define the gravity vector.
		b2Vec2 gravity;
		gravity.Set(0.0f, -50.0f);
		
		// Do we want to let bodies sleep?
		// This will speed up the physics simulation
		//bool doSleep = true;
		
		// Construct a world object, which will hold and simulate the rigid bodies.
		world = new b2World(gravity);
		
		world->SetContinuousPhysics(true);
        
		/*
         //Debug Draw functions
         m_debugDraw = new GLESDebugDraw( PTM_RATIO );
         world->SetDebugDraw(m_debugDraw);
         
         uint32 flags = 0;
         flags += b2DebugDraw::e_shapeBit;
         flags += b2DebugDraw::e_jointBit;
         flags += b2DebugDraw::e_aabbBit;
         //flags += b2DebugDraw::e_pairBit;
         //flags += b2DebugDraw::e_centerOfMassBit;
         m_debugDraw->SetFlags(flags);
         */
		
		// Define the ground body.
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(0, 0); // bottom-left corner
		
		// Call the body factory which allocates memory for the ground body
		// from a pool and creates the ground box shape (also from a pool).
		// The body is also added to the world.
		groundBody = world->CreateBody(&groundBodyDef);
		
		// Define the ground box shape.
		b2EdgeShape groundBox;		
		
		// bottom
		groundBox.Set(b2Vec2(0,0), b2Vec2(screenSize.width/PTM_RATIO,0));
		groundBody->CreateFixture(&groundBox,0);
		
		// top
		groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO*2), b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO*2));
		groundBody->CreateFixture(&groundBox,0);
		
		// left
		groundBox.Set(b2Vec2(0,screenSize.height/PTM_RATIO), b2Vec2(0,0));
		groundBody->CreateFixture(&groundBox,0);
		
		// right
		groundBox.Set(b2Vec2(screenSize.width/PTM_RATIO,screenSize.height/PTM_RATIO), b2Vec2(screenSize.width/PTM_RATIO,0));
		groundBody->CreateFixture(&groundBox,0);
		
		//背景画面
        NSString *path =[[NSString alloc] init];
        path = [NSString stringWithFormat:@"kira%d.pvr.gz",page];
        texture[sceneId] = [[CCTextureCache sharedTextureCache] addImage:path];
        spriteBack = [CCSprite spriteWithTexture:texture[sceneId] rect:CGRectMake(0,0,winSize.width,winSize.height)];
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack z:-1];
         
        //からくりマーク
        if(page == 2 || page == 3 || page == 4 || page == 6|| page == 7 || page == 12 || page == 13 || page == 16 || page==17){
            NSString *path2 =[[NSString alloc] init];
            path2 = [NSString stringWithFormat:@"karakuri.png"];
            CCTexture2D *texture2 = [[CCTextureCache sharedTextureCache] addImage:path2];
            CCSprite *mark = [CCSprite spriteWithTexture:texture2];
            mark.position = ccp(122,722);
            [self addChild: mark z:10];
        }

        
        
        //テキストOFFの時
        if(optionnum3[2] == 1){
            //ページ送り用コントロール
            CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                          itemWithNormalImage:@"arrow_box_left.pvr.gz" 
                                          selectedImage: @"arrow_box_left.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem1.tag =2;
            
            CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                          itemWithNormalImage:@"arrow_box_right.pvr.gz" 
                                          selectedImage: @"arrow_box_right.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem2.position = ccp(maryItem2.contentSize.width/2, 140);
            maryItem1.position = ccp(maryItem1.contentSize.width/2-maryItem2.contentSize.width,140);    
            maryItem2.tag =1;
            CCMenu *menu = [CCMenu menuWithItems:maryItem1, maryItem2,nil];
            menu.position = ccp(512,-100);
            [self addChild:menu z:10];
            
            id func =[CCCallFunc actionWithTarget:self selector:@selector(Playnarration)];
            id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],func,nil];
            
            [self runAction:action2];

            
            //テキストONの時
        }else{
            
            //テキストの背景
            textBG_Sprite = [CCSprite spriteWithFile:@"text_field.pvr.gz"];
            textBG_Sprite.position = ccp(512, 0-textBG_Sprite.contentSize.height);
            [self addChild:textBG_Sprite z:3];
            
            NSString *text = [SceneManager textForSceneId:sceneId];
            CCLabelTTF *label = [CCLabelTTF labelWithString:text
                                                   fontName:@"HiraKakuProN-W3"
                                                   fontSize:20];
            label.dimensions = CGSizeMake(850, 145);
            label.color = ccc3(255, 255, 255);
            label.position =  ccp(520, 115);
            [textBG_Sprite addChild:label z:3];

            
            id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, 110)];
            id func =[CCCallFunc actionWithTarget:self selector:@selector(Playnarration)];
            id ease1 =[CCEaseSineOut actionWithAction:move1];
            id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],ease1,nil];
            id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],func,nil];
            
            [self runAction:action2];
            [textBG_Sprite runAction:action1];
            
            //ページめくり+homeボタンの生成
            CCMenuItemImage *maryItem1 = [CCMenuItemImage 
                                          itemWithNormalImage:@"nextpage.pvr.gz" 
                                          selectedImage: @"nextpage.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem1.position = ccp(512-maryItem1.contentSize.width/2, -265);
            maryItem1.tag =1;
            
            CCMenuItemImage *maryItem2 = [CCMenuItemImage 
                                          itemWithNormalImage:@"prepage.pvr.gz" 
                                          selectedImage: @"prepage.pvr.gz"
                                          target:self
                                          selector:@selector(pressMenuItem:)];
            maryItem2.position = ccp(-512+maryItem2.contentSize.width/2, -265);
            maryItem2.tag =2;
            
            maryItem3 = [CCMenuItemImage 
                         itemWithNormalImage:@"arrow_down.pvr.gz" 
                         selectedImage: @"arrow_down.pvr.gz"
                         target:self
                         selector:@selector(pressMenuItem:)];
            maryItem3.position = ccp(330+maryItem3.contentSize.width/2, -142);
            maryItem3.tag =3;
            
            maryItem4 = [CCMenuItemImage 
                         itemWithNormalImage:@"arrow_up.pvr.gz" 
                         selectedImage: @"arrow_up.pvr.gz"
                         target:self
                         selector:@selector(pressMenuItem:)];
            maryItem4.position = ccp(330+maryItem4.contentSize.width/2, -142);
            maryItem4.tag =4;
            maryItem4.visible = false;
            
            CCMenu *menu = [CCMenu menuWithItems:maryItem1, maryItem2,maryItem3,maryItem4,nil];
            [textBG_Sprite addChild:menu z:15];
            
        }

        
        //ホームボタン
        CCMenuItemImage *maryItem5 = [CCMenuItemImage 
                                      itemWithNormalImage:@"home.png" 
                                      selectedImage: @"home.png"
                                      target:self
                                      selector:@selector(pressMenuItem:)];
        maryItem5.position = ccp(-460, 340);
        maryItem5.tag =5;
        CCMenu *menu2 = [CCMenu menuWithItems:maryItem5,nil];
        [self addChild:menu2 z:10];
		
        
        
        [self PageEffect];
		[self schedule: @selector(tick:)];
	}
	return self;
}

-(void) addNewSpriteWithCoords:(CGPoint)p:(NSString *)name
{
    touche = true;
    NSString *imagePath = [NSString stringWithFormat:@"%@.pvr.gz",name];
    
    CCSprite *sprite4 = [CCSprite spriteWithFile:imagePath];
	sprite4.position = ccp( p.x, p.y);
	[self addChild:sprite4];
    
    //動的ボディの定義
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
    
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	bodyDef.userData = sprite4;
	body = world->CreateBody(&bodyDef);
	
    //ボックスシェイプを定義
	b2PolygonShape dynamicBox;
    float boxhsize = sprite4.contentSize.height / PTM_RATIO;
    float boxwsize = sprite4.contentSize.width / PTM_RATIO;
    
    
	dynamicBox.SetAsBox(boxwsize*0.43f, boxhsize*0.45f);
    
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.2f;
    fixtureDef.restitution = 0.0;
	_paddleFixture = body->CreateFixture(&fixtureDef);
}

//首振り人形
-(void) addNewSpriteWithJoint:(CGPoint)p:(NSString *)name{
    touche = true;
    NSString *imagePath = [NSString stringWithFormat:@"%@.pvr.gz",name];
    
    CCSprite *sprite4 = [CCSprite spriteWithFile:imagePath];
	sprite4.position = ccp( p.x, p.y);
	[self addChild:sprite4];
    
    //動的ボディの定義
    b2Body* ground = NULL;
	b2BodyDef bodyDef;
    b2PolygonShape dynamicBox;
    
    ground = world->CreateBody(&bodyDef);
    ground->CreateFixture(&dynamicBox, 8.0f);
    
	bodyDef.type = b2_dynamicBody;
    
    //ボックスシェイプを定義
    float boxhsize = sprite4.contentSize.height/PTM_RATIO;
    float boxwsize = sprite4.contentSize.width/PTM_RATIO;    
    
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	bodyDef.userData = sprite4;
	body = world->CreateBody(&bodyDef);
    
	dynamicBox.SetAsBox(boxwsize*0.5f, boxhsize*0.5f);
    
    
    b2DistanceJointDef jd;
    b2Vec2 p1, p2, d;
    
    jd.frequencyHz = 3.0f;
    jd.dampingRatio = 0.0f;
    
    jd.bodyA = ground;
    jd.bodyB = body;
    
    jd.localAnchorA.Set(p.x/PTM_RATIO-boxwsize/2-10/PTM_RATIO, p.y/PTM_RATIO-boxhsize/2-10/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize/2, -boxhsize/2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[0] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize/2+10/PTM_RATIO, p.y/PTM_RATIO-boxhsize/2-10/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize/2, -boxhsize/2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[1] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize/2+10/PTM_RATIO, p.y/PTM_RATIO+boxhsize/2+10/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize/2, boxhsize/2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[2] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO-boxwsize/2-10/PTM_RATIO, p.y/PTM_RATIO+boxhsize/2+10/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize/2, boxhsize/2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[3] = world->CreateJoint(&jd);
    
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize/2+10/PTM_RATIO, p.y/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize/2, 0.0/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[4] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO-boxwsize/2-10/PTM_RATIO, p.y/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize/2, 0.0/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[5] = world->CreateJoint(&jd);
    
    
    // Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 2.0f;
	fixtureDef.friction = 0.8f;
    fixtureDef.restitution = 2.0f;
	_paddleFixture = body->CreateFixture(&fixtureDef);
}


//シーン2
-(void) addNewSpriteWithJoint2:(CGPoint)p:(CGPoint)p5:(NSString *)name:(NSString *)name2{
    touche = true;
    NSString *imagePath = [NSString stringWithFormat:@"%@.pvr.gz",name];
    NSString *imagePath2 = [NSString stringWithFormat:@"%@.pvr.gz",name2];
    
    CCSprite *sprite3 = [CCSprite spriteWithFile:imagePath2 ];
	sprite3.position = ccp( p.x, p.y);
	[self addChild:sprite3];
    
    CCSprite *sprite4 = [CCSprite spriteWithFile:imagePath];
	sprite4.position = ccp( p.x, p.y);
	[self addChild:sprite4];
    
    //動的ボディの定義
    b2Body* ground = NULL;
	b2BodyDef bodyDef;
    b2PolygonShape dynamicBox[2];
    
    ground = world->CreateBody(&bodyDef);
    ground->CreateFixture(&dynamicBox[0], 8.0f);
    
	bodyDef.type = b2_dynamicBody;
    
    //ボックスシェイプを定義
    float boxhsize = sprite4.contentSize.height / PTM_RATIO*0.4f;
    float boxwsize = sprite4.contentSize.width / PTM_RATIO*0.4f;
    float boxhsize2 = sprite3.contentSize.height / PTM_RATIO*0.4f;
    float boxwsize2 = sprite3.contentSize.width / PTM_RATIO*0.4f;
    
    
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	bodyDef.userData = sprite4;
	m_bodies[1] = world->CreateBody(&bodyDef);
	dynamicBox[1].SetAsBox(boxwsize, boxhsize);
    
	bodyDef.position.Set(p5.x/PTM_RATIO, p5.y/PTM_RATIO);
    bodyDef.userData = sprite3;
    m_bodies[0] = world->CreateBody(&bodyDef);
    dynamicBox[0].SetAsBox(boxwsize2, boxhsize2);
    
    
    
    b2DistanceJointDef jd;
    b2Vec2 p1, p2, d;
    
    jd.frequencyHz = 3.0f;
    jd.dampingRatio = 0.0f;
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+80/PTM_RATIO, p.y/PTM_RATIO-boxhsize-80/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize, -boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[0] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+80/PTM_RATIO, p.y/PTM_RATIO-boxhsize-80/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize, -boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[1] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+80/PTM_RATIO, p.y/PTM_RATIO+boxhsize+80/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[2] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+80/PTM_RATIO, p.y/PTM_RATIO+boxhsize+80/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[3] = world->CreateJoint(&jd);
    
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+80/PTM_RATIO, p.y/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize/2, 0.0/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[4] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[1];
    jd.localAnchorA.Set(p.x/PTM_RATIO-boxwsize, p.y/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize, 0.0/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[5] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p5.x/PTM_RATIO+boxwsize2+80/PTM_RATIO, p5.y/PTM_RATIO-boxhsize2-80/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize2, -boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[7] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p5.x/PTM_RATIO+boxwsize2+80/PTM_RATIO, p5.y/PTM_RATIO-boxhsize2-80/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize2, -boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[8] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p5.x/PTM_RATIO+boxwsize2+80/PTM_RATIO, p5.y/PTM_RATIO+boxhsize2+80/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize2, boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[9] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p5.x/PTM_RATIO+boxwsize2+80/PTM_RATIO, p5.y/PTM_RATIO+boxhsize2+80/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize2, boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[10] = world->CreateJoint(&jd);
    
    
    // Define the dynamic body fixture.
    for (int i=0; i<2; i++) {
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &dynamicBox[i];
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 1.5f;
        fixtureDef.restitution = 2.0f;
        _paddleFixture2[i] = m_bodies[i]->CreateFixture(&fixtureDef);
        m_bodies[i]->CreateFixture(&dynamicBox[i],1.0f);
    }

}

//シーン17
-(void) addNewSpriteWithJoint3:(CGPoint)p:(CGPoint)p5:(NSString *)name:(NSString *)name2{
    
    NSString *imagePath = [NSString stringWithFormat:@"%@.pvr.gz",name];
    NSString *imagePath2 = [NSString stringWithFormat:@"%@.pvr.gz",name2];
    
    sprite2[0] = [CCSprite spriteWithFile:imagePath2 ];
    sprite2[0].position = ccp( p.x, p.y);
	[self addChild: sprite2[0]];
    
    sprite2[1] = [CCSprite spriteWithFile:imagePath];
	sprite2[1].position = ccp( p.x, p.y);
	[self addChild:sprite2[1]];
    
    //動的ボディの定義
    b2Body* ground = NULL;
	b2BodyDef bodyDef;
    b2PolygonShape dynamicBox;
    b2PolygonShape dynamicBox2;
    
    ground = world->CreateBody(&bodyDef);
    ground->CreateFixture(&dynamicBox, 8.0f);
    
	bodyDef.type = b2_dynamicBody;
    
    //ボックスシェイプを定義
    float boxhsize = sprite2[1].contentSize.height / PTM_RATIO*0.4f;
    float boxwsize = sprite2[1].contentSize.width / PTM_RATIO*0.4f;
    float boxhsize2 = sprite2[0].contentSize.height / PTM_RATIO*0.4f;
    float boxwsize2 = sprite2[0].contentSize.width / PTM_RATIO*0.4f;
    
    
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	bodyDef.userData = sprite2[1];
	body = world->CreateBody(&bodyDef);
	dynamicBox.SetAsBox(boxwsize, boxhsize);
    
	bodyDef.position.Set(p5.x/PTM_RATIO, p5.y/PTM_RATIO);
    bodyDef.userData = sprite2[0];
    m_bodies[0] = world->CreateBody(&bodyDef);
    dynamicBox2.SetAsBox(boxwsize2, boxhsize2);
    
    
    
    b2DistanceJointDef jd;
    b2Vec2 p1, p2, d;
    
    jd.frequencyHz = 3.0f;
    jd.dampingRatio = 0.0f;
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+50/PTM_RATIO, p.y/PTM_RATIO-boxhsize-200/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize, -boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[0] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+50/PTM_RATIO, p.y/PTM_RATIO-boxhsize-200/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize, -boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[1] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+50/PTM_RATIO, p.y/PTM_RATIO+boxhsize+200/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[2] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+50/PTM_RATIO, p.y/PTM_RATIO+boxhsize+200/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize, boxhsize);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[3] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO+boxwsize+120/PTM_RATIO, p.y/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize/2, 0.0/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[4] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = body;
    jd.localAnchorA.Set(p.x/PTM_RATIO-boxwsize+50/PTM_RATIO, p.y/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize, 0.0/PTM_RATIO);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[5] = world->CreateJoint(&jd);
    
    
    
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p5.x/PTM_RATIO-boxwsize2-50/PTM_RATIO, p5.y/PTM_RATIO+boxhsize2+100/PTM_RATIO);
    jd.localAnchorB.Set(-boxwsize2+40/PTM_RATIO, boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[6] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p5.x/PTM_RATIO+boxwsize2+50/PTM_RATIO, p5.y/PTM_RATIO+boxhsize2+100/PTM_RATIO);
    jd.localAnchorB.Set(boxwsize2-40/PTM_RATIO, boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[7] = world->CreateJoint(&jd);
    
    jd.bodyA = ground;
    jd.bodyB = m_bodies[0];
    jd.localAnchorA.Set(p5.x/PTM_RATIO, p5.y/PTM_RATIO-boxhsize2-100/PTM_RATIO);
    jd.localAnchorB.Set(0.0, boxhsize2);
    p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
    p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
    d = p2 - p1;
    jd.length = d.Length();
    m_joints[8] = world->CreateJoint(&jd);
    
    
    
    // Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density =1.0f;
	fixtureDef.friction = 2.0f;
    fixtureDef.restitution = 0.2f;
	_paddleFixture2[0] = body->CreateFixture(&fixtureDef);
    body ->CreateFixture(&fixtureDef);
    
    b2FixtureDef fixtureDef2;
	fixtureDef2.shape = &dynamicBox2;
	fixtureDef2.density = 1.0f;
	fixtureDef2.friction = 2.0f;
    fixtureDef2.restitution = 0.2f;
	_paddleFixture2[1] = m_bodies[0]->CreateFixture(&fixtureDef2);
    m_bodies[0]->CreateFixture(&fixtureDef2);
    
    touche = true;
}

-(void) addNewSliderCrank:(CGPoint)p:(NSString *)name{
    touche = true;
    NSString *imagePath = [NSString stringWithFormat:@"%@.pvr.gz",name];
    CCSprite *sprite4 = [CCSprite spriteWithFile:imagePath];
	sprite4.position = ccp( p.x, p.y);
	[self addChild:sprite4];
    
    float boxhsize = sprite4.contentSize.height / PTM_RATIO;
    float boxwsize = sprite4.contentSize.width / PTM_RATIO;
    
    b2Body* ground = NULL;
    b2BodyDef bd;
    ground = world->CreateBody(&bd);
    
    b2PolygonShape shape;
    ground->CreateFixture(&shape, 0.0f);
    
    b2Body* prevBody = ground;
    
    // Define crank.
    shape.SetAsBox(70/PTM_RATIO, 40/PTM_RATIO);
    
    bd.type = b2_dynamicBody;
    bd.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO-420/PTM_RATIO);
    body = world->CreateBody(&bd);
    body->CreateFixture(&shape, 2.0f);
    
    b2RevoluteJointDef rjd;
    rjd.Initialize(prevBody, body, b2Vec2(p.x/PTM_RATIO, p.y/PTM_RATIO-460/PTM_RATIO));
    rjd.motorSpeed = 1.0f * b2_pi;
    rjd.maxMotorTorque = 120000.0f;
    rjd.enableMotor = true;
    m_joint1 = (b2RevoluteJoint*)world->CreateJoint(&rjd);
    
    prevBody = body;
    
    // Define follower.
    shape.SetAsBox(70/PTM_RATIO, 70/PTM_RATIO);
    
    bd.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO-330/PTM_RATIO);
    body = world->CreateBody(&bd);
    body->CreateFixture(&shape, 2.0f);
    
    rjd.Initialize(prevBody, body, b2Vec2(p.x/PTM_RATIO, p.y/PTM_RATIO-400/PTM_RATIO));
    rjd.enableMotor = false;
    world->CreateJoint(&rjd);
    
    prevBody = body;
    
    // Define piston
    shape.SetAsBox(boxwsize*0.5f, boxhsize*0.34f);
    
    bd.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO-70/PTM_RATIO);
    bd.userData = sprite4;
    
    body = world->CreateBody(&bd);
    body->CreateFixture(&shape, 2.0f);
    
    rjd.Initialize(prevBody, body, b2Vec2(p.x/PTM_RATIO, p.y/PTM_RATIO-70/PTM_RATIO));
    world->CreateJoint(&rjd);
    
    b2PrismaticJointDef pjd;
    pjd.Initialize(ground, body, b2Vec2(p.x/PTM_RATIO, p.y/PTM_RATIO-70/PTM_RATIO), b2Vec2(0.0f, 1.0f));
    
    pjd.maxMotorForce = 20000.0f;
    pjd.lowerTranslation = -5.7;
    pjd.upperTranslation = 0.5;
    pjd.enableLimit = true;
    pjd.enableMotor = true;
    m_joint2 = (b2PrismaticJoint*)world->CreateJoint(&pjd);
    
    
    // Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;	
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.0f;
    fixtureDef.restitution = 1.0f;
	_paddleFixture = body->CreateFixture(&fixtureDef);
}


//メニューボタン処理
#pragma mark イベント処理
- (void)pressMenuItem:(id)sender {
    if([sender tag] == 1){
        next = YES;
        [[CCDirector sharedDirector] setDepthTest:YES];
        if(page == 5){
            [Movie_Sprite[2]  removeFromParentAndCleanup:YES];
        }
        if(page == 18){
            [SoundPlayer bgmStop2:setteino[0]];
        }
        
        //FadeOut
        [[SimpleAudioEngine sharedEngine] stopEffect:se];

		[self FadeOut];
       
        
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseA
                                                      next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];        
        
    }else if([sender tag] == 2 && page > 1){
        next = NO;
        if(page == 5){
            [Movie_Sprite[2]  removeFromParentAndCleanup:YES];
        }
        if (page == 2) {
            [SoundPlayer bgmStop2:optionnum3[0]];
        }
        [[CCDirector sharedDirector] setDepthTest:YES];
        //FadeOut
        [[SimpleAudioEngine sharedEngine] stopEffect:se];

        [self FadeOut];
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                    choice:kSceneChoiseB
                                                      next:false];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
        
       

                
    }else if([sender tag] == 3){
        [[CCDirector sharedDirector] setDepthTest:NO];
        
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, -80)];
        CCEaseExponentialOut *ease1 =[CCEaseExponentialOut actionWithAction:move1];
        [textBG_Sprite runAction:ease1];
        maryItem3.visible =false;
        maryItem4.visible =true;
        
    }else if([sender tag] == 4){        
        id move1 = [CCMoveTo actionWithDuration:0.5f position:ccp(512, 110)];
        CCEaseExponentialOut *ease1 =[CCEaseExponentialOut actionWithAction:move1];
        [textBG_Sprite runAction:ease1];
        maryItem3.visible =true;
        maryItem4.visible =false;
        
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] 
                                  initWithTitle:@"タイトルへ戻りますか？"
                                  message:nil                             
                                  delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"はい",@"いいえ",nil];
        [alertView show];
        [alertView release];
        
        
    }
}

//タイトルへ戻るアラート
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            
            if(page == 5){
                [Movie_Sprite[2]  removeFromParentAndCleanup:YES];
            }

            
            CDLongAudioSource *player2 = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
            CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player2 interpolationType:kIT_Exponential startVal:player2.volume endVal:0.0];
            [fader setStopTargetWhenComplete:YES];
            //Create a property modifier action to wrap the fader
            CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:0.5 modifier:fader];
            [fader release];//Action will retain
            id actionCallFuncN = [CCCallFuncN actionWithTarget:self selector:@selector(actionComplete)];
            [[CCDirector sharedDirector].actionManager  addAction:[CCSequence actions:action,actionCallFuncN, nil] target:player2 paused:NO];
            
            [SoundPlayer bgmStop2:optionnum3[0]];
    }
}

//フェードアウト
-(void)FadeOut{
    CDLongAudioSource *player2 = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
    CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player2 interpolationType:kIT_Exponential startVal:player2.volume endVal:0.0];
    [fader setStopTargetWhenComplete:YES];
    //Create a property modifier action to wrap the fader
    CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:0.5 modifier:fader];
    [fader release];//Action will retain
    id actionCallFuncN = [CCCallFuncN actionWithTarget:self selector:@selector(actionComplete2)];
    [[CCDirector sharedDirector].actionManager  addAction:[CCSequence actions:action,actionCallFuncN, nil] target:player2 paused:NO];
}

- (void)actionComplete{
    [[CCDirector sharedDirector] setDepthTest:YES];

    CCScene *newScene = [SceneManager nextSceneOfScene:self.parent
                                                choice:kSceneChoiseC next:true];
    [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
    
}

- (void)actionComplete2{
    [player stop];
}
//ナレーション
-(void)Playnarration{
    //ipad2バグ調整
    if(page == 1){
        [[CCDirector sharedDirector] setDepthTest:NO];
    }
    if(setteino[0] == 0){
        NSString *narration = [[NSString alloc] initWithFormat:@"scene%d.caf",page];
        player = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
        [player load:narration];
        player.volume = 1.0f;
        [player setNumberOfLoops:0];
        [player play];
        [narration release];
    }
}

#pragma mark ギミック
-(void)PageEffect{
    if(page == 1){
        
        //BGM読み込み
        if (next == YES) {
            [SoundPlayer bgmPlay:optionnum3[1]:0];
        }
        
        
        //効果音の読み込み
        [SoundPlayer PlaySE:optionnum3[1] :0];
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira1_oshiro.pvr.gz"];
        sprite3.position = ccp(545, 465);
        [self addChild:sprite3 z:1];
        
        CCSprite *sprite4 = [CCSprite spriteWithFile:@"kira1_yane.pvr.gz"];
        sprite4.position = ccp(440, 220);
        [self addChild:sprite4 z:2];
        
        id jumpBy = [CCJumpBy actionWithDuration:0.4f position:ccp(0,0) height:150 jumps:1]; 
        id Rote = [CCRotateBy actionWithDuration: 0.2f angle:10]; 
        id Rote2 = [CCRotateBy actionWithDuration: 0.2f angle:-10]; 
        CCEaseBounceOut *bounce = [CCEaseBounceOut actionWithAction:Rote2];
        id action = [CCSpawn actions:jumpBy,Rote, nil];
        id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],action,bounce,[CCShaky3D actionWithRange:1 shakeZ:NO grid:ccg(1,1) duration:0.0f],nil];
        id action3 = [CCSequence actions:
                      [CCDelayTime actionWithDuration:2.2f],
                      [CCShaky3D actionWithRange:20 shakeZ:NO grid:ccg(8,8) duration:1.0f],
                      [CCShaky3D actionWithRange:1 shakeZ:NO grid:ccg(1,1) duration:0.0f],
                      nil];
        [sprite4 runAction:action2];
        [sprite3 runAction:action3];
    }else if(page == 2){
        
        sprite2[0] = [CCSprite spriteWithFile:@"kira02_akou.pvr.gz"];
        sprite2[0].position = ccp(1424, 700);
        [self addChild: sprite2[0]];
        
        sprite2[1] = [CCSprite spriteWithFile:@"kira02_kouzukenosuke.pvr.gz"];
        sprite2[1].position = ccp(1024, 460);
        [self addChild:sprite2[1]];
        
        id move1 = [CCMoveTo actionWithDuration:1.0f position:ccp(720, 460)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        id fanc = [CCCallFunc actionWithTarget:self selector:@selector(sece2)];
        id action =[CCSequence actions:ease,fanc,nil];
        id move2 = [CCMoveTo actionWithDuration:1.0f position:ccp(300, 330)];
        id ease2 = [CCEaseSineOut actionWithAction:move2];
        [sprite2[0] runAction:action];
        [sprite2[1] runAction:ease2];
        
        
        
        
    }else if(page == 3){
        
        touche = true;
        CCSprite *sprite6 = [CCSprite spriteWithFile:@"kira03_akou.pvr.gz"];
        sprite6.position = ccp(225, 370);
        [self addChild:sprite6];
        
        CCSprite *sprite7 = [CCSprite spriteWithFile:@"kira03_kouzuke.pvr.gz"];
        sprite7.position = ccp(775, 420);
        [self addChild:sprite7];
        
        CCSprite *sprite8 = [CCSprite spriteWithFile:@"kira03_tawara.pvr.gz"];
        sprite8.position = ccp(500, 220);
        [self addChild:sprite8];
        
        sprite2[0] = [CCSprite spriteWithFile:@"kira03_koban.pvr.gz"];
        sprite2[0].position = ccp(800, 320);
        [self addChild:sprite2[0]];
        
        
        CCMenuItemImage *koban = [CCMenuItemImage 
                                  itemWithNormalImage:@"kira03_te.pvr.gz" 
                                  selectedImage: @"kira03_te.pvr.gz"
                                  target:self
                                  selector:@selector(onMusicSound:)];
        koban.position =ccp(283, -145);
        koban.tag =4;
        
        CCMenu * menu = [CCMenu menuWithItems:koban, nil];
        [self addChild:menu];
        
    }else if(page == 4){
        if(!next){
            [SoundPlayer bgmStop:optionnum3[1]:1];
            [SoundPlayer bgmPlay:optionnum3[1]:0];
        }
        CGPoint location = CGPointMake(525, 550);
        CCSprite *sprite4 = [CCSprite spriteWithFile:@"kira04_eri.pvr.gz"];
        sprite4.position = ccp(location.x-13, location.y-249);
        [self addChild:sprite4];
        
        NSString *name = [[NSString alloc] initWithString:@"kira04_kao"];
        [self  addNewSliderCrank:location:name];
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira04_dou.pvr.gz"];
        sprite3.position = ccp(525, 156);
        [self addChild:sprite3];
        
        
    }else if(page == 5){
        //BGM読み込み
        [SoundPlayer bgmStop:optionnum3[1]:0];
        [SoundPlayer bgmPlay:optionnum3[1]:1];
        
        CCSprite *sprite1 = [CCSprite spriteWithFile:@"kira05_kira.pvr.gz"];
        sprite1.position = ccp(512, 0);
        [self addChild:sprite1];
        
        id move1 = [CCMoveTo actionWithDuration:1.5f position:ccp(512, 300)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        [sprite1 runAction:ease];
        
        
    }else if(page == 6){
        if(optionnum3[1] == 0){
            //効果音の読み込み
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"uma.caf"];
        }
        
        sprite2[0] = [CCSprite spriteWithFile:@"kira6_uma.pvr.gz"];
        sprite2[0].position = ccp(1424, 388);
        [self addChild: sprite2[0]];
        
        id move1 = [CCMoveTo actionWithDuration:3.8f position:ccp(551, 387)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        id fanc = [CCCallFunc actionWithTarget:self selector:@selector(sece6)];
        id action =[CCSequence actions:ease,fanc,nil];
        [sprite2[0] runAction:action];
        
        
    }else if(page == 7){
        
        if(!next){
            [SoundPlayer bgmStop:optionnum3[1]:2];
            [SoundPlayer bgmPlay:optionnum3[1]:1];
        }
        if(optionnum3[1] == 0){
            //効果音の読み込み
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"gensou.caf"];
        }
        
        CCSprite *sprite1 = [CCSprite spriteWithFile:@"kira07_hikari.pvr.gz"];
        sprite1.anchorPoint = CGPointMake(0.5f, 0.5f);
        sprite1.position = ccp(522, 480);
        sprite1.scale=0.6f;
        [self addChild:sprite1];
        
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira07_kannon.pvr.gz"];
        sprite3.anchorPoint = CGPointMake(0.5f, 0.5f);
        sprite3.position = ccp(522, 380);
        sprite3.scale=0.6f;
        [self addChild:sprite3];
        
        id move1 = [CCFadeIn actionWithDuration:3.0f];
        id move2 = [CCScaleTo actionWithDuration:3.0f scale:1.0f];
		id fanc = [CCCallFunc actionWithTarget:self selector:@selector(soundEffect)];
        
        id move3 = [CCFadeIn actionWithDuration:4.0f];
        id move4 = [CCScaleBy actionWithDuration:4.0f scale:2.0f];
        id move5 = [CCFadeOut actionWithDuration:4.0f];
        id move6 = [move4 reverse];
        
        
        CCEaseSineOut *ease1 =[CCEaseSineOut actionWithAction:move2];
        CCEaseSineOut *ease2 =[CCEaseSineOut actionWithAction:move4];
        
        id seq = [CCSequence actions:[CCSpawn actions:move1,ease1,nil],fanc,nil];
        id seq2 = [CCSequence actions:[CCSpawn actions:move3,ease2,nil],[CCSpawn actions:move5,move6,nil],nil];
        id rep2 = [CCRepeatForever actionWithAction: [[seq2 copy] autorelease]];
        
        [sprite3 runAction:seq];
        [sprite1 runAction:rep2];
        
    }
    else if(page == 8){
        //BGM読み込み
        if(!next){
            [SoundPlayer bgmStop:optionnum3[1]:3];
            [SoundPlayer bgmPlay:optionnum3[1]:2];
        }else{
            [SoundPlayer bgmStop:optionnum3[1]:1];
            [SoundPlayer bgmPlay:optionnum3[1]:2];
        }
        
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira08_right_dou.pvr.gz"];
        sprite3.position = ccp(745, 200);
        [self addChild:sprite3];
        
        CCSprite *sprite4 = [CCSprite spriteWithFile:@"kira08_right_kao.pvr.gz"];
        sprite4.anchorPoint = CGPointMake(0.95f, 0.1f);
        sprite4.position = ccp(788, 280);
        [self addChild:sprite4];
        
        CCSprite *sprite5 = [CCSprite spriteWithFile:@"kira08_right_dou_ue.pvr.gz"];
        sprite5.position = ccp(805, 237);
        [self addChild:sprite5];
        
        CCSprite *sprite6 = [CCSprite spriteWithFile:@"kira08_left_dou.pvr.gz"];
        sprite6.position = ccp(295, 175);
        [self addChild:sprite6];
        
        CCSprite *sprite7 = [CCSprite spriteWithFile:@"kira08_left_kao.pvr.gz"];
        sprite7.anchorPoint = CGPointMake(0.55f, 0.45f);
        sprite7.position = ccp(280, 310);
        [self addChild:sprite7];
        
        CCSprite *sprite8 = [CCSprite spriteWithFile:@"kira08_left_ue.pvr.gz"];
        sprite8.position = ccp(220, 205);
        [self addChild:sprite8];
        
        
        id Rote = [CCRotateBy actionWithDuration: 2.0f angle:-10]; 
        id Rote2 = [CCRotateBy actionWithDuration: 2.0f angle:10]; 
        CCEaseBounceOut *bounce = [CCEaseBounceOut actionWithAction:Rote2];
        
        id action3 = [CCSequence actions:
                      [CCDelayTime actionWithDuration:1.0f],Rote,bounce,nil];
        id rep2 = [CCRepeatForever actionWithAction: [[action3 copy] autorelease]];
        
        
        id Rote3 = [CCRotateBy actionWithDuration: 2.0f angle:10]; 
        id Rote4 = [CCRotateBy actionWithDuration: 2.0f angle:-10]; 
        CCEaseBounceOut *bounce2 = [CCEaseBounceOut actionWithAction:Rote4];
        
        id action4 = [CCSequence actions:
                      [CCDelayTime actionWithDuration:1.0f],Rote3,bounce2,nil];
        id rep3 = [CCRepeatForever actionWithAction: [[action4 copy] autorelease]];
        
        [sprite4 runAction:rep2];
        [sprite7 runAction:rep3];
        
    }
    else if(page == 9){
        
        //効果音の読み込み
        
        if(!next){
            [SoundPlayer bgmStop:optionnum3[1]:4];
            [SoundPlayer bgmPlay:optionnum3[1]:3];
        }else{
            [SoundPlayer bgmStop:optionnum3[1]:2];
            [SoundPlayer bgmPlay:optionnum3[1]:3];
        }
        
        
        sprite2[0] = [CCSprite spriteWithFile:@"kira09_nami1.pvr.gz"];
        sprite2[0].position = ccp(630, 600);
        sprite2[0].opacity =0.0f;
        [self addChild:sprite2[0] z:4];
        
        sprite2[1] = [CCSprite spriteWithFile:@"kira09_nami2.pvr.gz"];
        sprite2[1].opacity =0.0f;
        sprite2[1].position = ccp(550, 550);
        [self addChild:sprite2[1] z:3];
        
        sprite2[2] = [CCSprite spriteWithFile:@"kira09_nami3.pvr.gz"];
        sprite2[2].opacity =0.0f;
        sprite2[2].position = ccp(450, 430);
        [self addChild:sprite2[2] z:2];
        
        sprite2[3] = [CCSprite spriteWithFile:@"kira09_nami4.pvr.gz"];
        sprite2[3].opacity =0.0f;
        sprite2[3].position = ccp(340, 310);
        [self addChild:sprite2[3] z:1];
        
        sprite2[4] = [CCSprite spriteWithFile:@"kira09_kaminari.pvr.gz"];
        sprite2[4].position = ccp(660, 620);
        [self addChild:sprite2[4] z:5];
        
        sprite2[5] = [CCSprite spriteWithFile:@"kira09_ame.pvr.gz"];
        sprite2[5].position = ccp(200, 600);
        [self addChild:sprite2[5] z:6];
        
        
		id fanc1 = [CCCallFunc actionWithTarget:self selector:@selector(wave)];
        id Fade1 = [CCFadeIn actionWithDuration: 1.0f]; 
        id Move1 = [CCMoveTo actionWithDuration: 1.0f position:ccp(595,570)];
        id ease1 = [CCEaseSineOut actionWithAction:Move1];
        id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],[CCSpawn actions:Fade1,ease1,nil],fanc1,nil];
        
        id fanc2 = [CCCallFunc actionWithTarget:self selector:@selector(wave2)];
        id Fade2 = [CCFadeIn actionWithDuration: 1.0f]; 
        id Move2 = [CCMoveTo actionWithDuration: 1.0f position:ccp(520,500)];
        id ease2 = [CCEaseSineOut actionWithAction:Move2];
        id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],[CCSpawn actions:Fade2,ease2,nil],fanc2,nil];
        
        id fanc3 = [CCCallFunc actionWithTarget:self selector:@selector(wave3)];
        id Fade3 = [CCFadeIn actionWithDuration: 1.0f]; 
        id Move3 = [CCMoveTo actionWithDuration: 1.0f position:ccp(390,350)];
        id ease3 = [CCEaseSineOut actionWithAction:Move3];
        id action3 = [CCSequence actions:[CCDelayTime actionWithDuration:3.0f],[CCSpawn actions:Fade3,ease3,nil],fanc3,nil];
        
        id fanc4 = [CCCallFunc actionWithTarget:self selector:@selector(wave4)];
        id Fade4 = [CCFadeIn actionWithDuration: 1.0f]; 
        id Move4 = [CCMoveTo actionWithDuration: 1.0f position:ccp(300,230)];
        id ease4 = [CCEaseSineOut actionWithAction:Move4];
        id action4 = [CCSequence actions:[CCDelayTime actionWithDuration:4.0f],[CCSpawn actions:Fade4,ease4,nil],fanc4,nil];
        
        id brink = [CCBlink actionWithDuration:0.5 blinks:4];
        
        id Sec = [CCSequence actions:brink,[CCDelayTime actionWithDuration:5.0f], nil];
        id rep = [CCRepeatForever actionWithAction: [[Sec copy] autorelease]];
        
        
        
        [sprite2[0] runAction:action1];
        [sprite2[1] runAction:action2];
        [sprite2[2] runAction:action3];
        [sprite2[3] runAction:action4];
        [sprite2[4] runAction:rep];
        
        
        
    }
    else if(page == 10){
        if(!next){
            [SoundPlayer bgmStop:optionnum3[1]:1];
            [SoundPlayer bgmPlay:optionnum3[1]:4];
        }else{
            [SoundPlayer bgmStop:optionnum3[1]:3];
            [SoundPlayer bgmPlay:optionnum3[1]:4];
        }
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira10_bouzu_dou.pvr.gz"];
        sprite3.position = ccp(620, 330);
        [self addChild:sprite3 z:1];
        
        CCSprite *sprite4 = [CCSprite spriteWithFile:@"kira10_bouzu_head.pvr.gz"];
        sprite4.position = ccp(613, 460);
        [self addChild:sprite4 z:2];
        
        CCSprite *sprite5 = [CCSprite spriteWithFile:@"kira10_kodomo_dou.pvr.gz"];
        sprite5.position = ccp(350, 350);
        [self addChild:sprite5 z:3];
        
        CCSprite *sprite6 = [CCSprite spriteWithFile:@"kira10_kodomo_head.pvr.gz"];
        sprite6.position = ccp(330, 545);
        [self addChild:sprite6 z:4];
        
        CCSprite *sprite7 = [CCSprite spriteWithFile:@"kira10_kodomo_namida.pvr.gz"];
        sprite7.position = ccp(310, 529);
        [self addChild:sprite7 z:5];
        
        
        id Rote = [CCMoveBy actionWithDuration: 2.0f position:ccp(0,-8)]; 
        id Rote2 = [Rote reverse]; 
        CCEaseElasticOut *bounce = [CCEaseElasticOut actionWithAction:Rote2];
        
        id action3 = [CCSequence actions:
                      [CCDelayTime actionWithDuration:1.0f],Rote,bounce,nil];
        id rep2 = [CCRepeatForever actionWithAction: [[action3 copy] autorelease]];
        
        
        id Rote3 = [CCMoveBy actionWithDuration: 1.5f position:ccp(0,-50)]; 
        id Rote4 = [Rote3 reverse];
        id Rote5 = [CCFadeOut actionWithDuration: 1.5f];
        CCEaseExponentialIn *SineOut = [CCEaseExponentialIn actionWithAction:Rote3];
        CCEaseExponentialIn *SineOut2 = [CCEaseExponentialIn actionWithAction:Rote5];
        id action4 = [CCSequence actions:[CCSpawn actions:SineOut,SineOut2,nil],Rote4,nil];
        id rep3 = [CCRepeatForever actionWithAction: [[action4 copy] autorelease]];
        
        [sprite4 runAction:rep2];
        [sprite7 runAction:rep3];
        
    }
    else if(page == 11){
        //BGM読み込み
        if(!next){
        }else{
            [SoundPlayer bgmStop:optionnum3[1]:4];
            [SoundPlayer bgmPlay:optionnum3[1]:1];
        }
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira11_left_dou.pvr.gz"];
        sprite3.position = ccp(200, 330);
        [self addChild:sprite3];
        
        CCSprite *sprite4 = [CCSprite spriteWithFile:@"kira11_left_head.pvr.gz"];
        sprite4.position = ccp(260, 500);
        [self addChild:sprite4];
        
        CCSprite *sprite5 = [CCSprite spriteWithFile:@"kira11_left_shoulder.pvr.gz"];
        sprite5.position = ccp(210, 435);
        [self addChild:sprite5];
        
        CCSprite *sprite6 = [CCSprite spriteWithFile:@"kira11_left_hand.pvr.gz"];
        sprite6.anchorPoint = CGPointMake(0.4f, 0.90f);
        sprite6.position = ccp(0,0);
        sprite6.position = ccp(110, 315);
        [self addChild:sprite6];
        
        
        
        CCSprite *sprite8 = [CCSprite spriteWithFile:@"kira11_right_dou.pvr.gz"];
        sprite8.position = ccp(800, 320);
        [self addChild:sprite8];
        
        CCSprite *sprite7 = [CCSprite spriteWithFile:@"kira11_right_head.pvr.gz"];
        sprite7.position = ccp(792, 515);
        [self addChild:sprite7];
        
        
        id Rote = [CCMoveBy actionWithDuration: 2.0f position:ccp(0,-8)]; 
        id Rote2 = [Rote reverse]; 
        
        id action3 = [CCSequence actions:
                      [CCDelayTime actionWithDuration:1.0f],Rote,Rote2,nil];
        id rep2 = [CCRepeatForever actionWithAction: [[action3 copy] autorelease]];
        
        id Rote3 = [CCRotateBy actionWithDuration: 3.0f angle:-30]; 
        id Rote4 = [Rote3 reverse];
        CCEaseExponentialOut *SineOut = [CCEaseExponentialOut actionWithAction:Rote3];
        id action4 = [CCSequence actions:SineOut,Rote4,nil];
        id rep3 = [CCRepeatForever actionWithAction: [[action4 copy] autorelease]];
        
        id Rote5 = [CCMoveBy actionWithDuration: 2.0f position:ccp(0,-8)]; 
        id Rote6 = [Rote5 reverse]; 
        id action5 = [CCSequence actions:
                      [CCDelayTime actionWithDuration:3.0f],Rote5,Rote6,nil];
        id rep4 = [CCRepeatForever actionWithAction: [[action5 copy] autorelease]];
        
        [sprite4 runAction:rep2];
        [sprite6 runAction:rep3];
        [sprite7 runAction:rep4];
    }
    else if(page == 12){
        
        sprite2[0] = [CCSprite spriteWithFile:@"kira12_otoko.pvr.gz"];
        sprite2[0].position = ccp(555, -100);
        [self addChild: sprite2[0]];
        
        id move1 = [CCMoveTo actionWithDuration:1.0f position:ccp(555, 284)];
        id ease = [CCEaseSineOut actionWithAction:move1];
        id fanc = [CCCallFunc actionWithTarget:self selector:@selector(sece12)];
        id action =[CCSequence actions:ease,fanc,nil];
        [sprite2[0] runAction:action];
        
    }
    
    else if(page == 13){
        touche = true;
        
        if(optionnum3[1] == 0){
            //効果音
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"wasse.caf"];
        }
        
        
        CCSprite *sprite1 = [CCSprite spriteWithFile:@"kira13_ishi_center.pvr.gz"];
        sprite1.position = ccp(550, 300);
        [self addChild:sprite1];
        
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira13_ishi_left.pvr.gz"];
        sprite3.position = ccp(250, 300);
        [self addChild:sprite3];
        
        CCSprite *sprite4 = [CCSprite spriteWithFile:@"kira13_ishi_right.pvr.gz"];
        sprite4.position = ccp(812, 300);
        [self addChild:sprite4];
    }
    else if(page == 14){
        
        if(optionnum3[1] == 0){
            //効果音
            se = [[SimpleAudioEngine sharedEngine] playEffect:@"kansei.caf"];
        }
        
        
        spriteBack.anchorPoint = CGPointMake(0.5f, 0.5f);
        spriteBack.position = ccp(512.0f,768*0.5f);
        id move = [CCScaleTo actionWithDuration:0 scale:1.3f];
        id move2 = [CCScaleTo actionWithDuration:5 scale:1.0f];
        id action1 = [CCSequence actions:move,move2,nil];
        [spriteBack runAction:action1];
        
    }else if(page == 15){
        if(!next){
            //BGM読み込み
            [SoundPlayer bgmStop:optionnum3[1]:0];
            [SoundPlayer bgmPlay:optionnum3[1]:1];
        }
        
        CCSprite *sprite6 = [CCSprite spriteWithFile:@"kira15_kouzuke.pvr.gz"];
        sprite6.position = ccp(1130, 612);
        [self addChild:sprite6];
        
        CCSprite *sprite7 = [CCSprite spriteWithFile:@"kira15_tatami.pvr.gz"];
        sprite7.position = ccp(-200, 500);
        [self addChild:sprite7];
        
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira15_ude.pvr.gz"];
        sprite3.anchorPoint = CGPointMake(1.0f, 1.0f);
        sprite3.position = ccp(1130, 565);
        [self addChild:sprite3];
        
        CCSprite *sprite1 = [CCSprite spriteWithFile:@"kira15_dou.pvr.gz"];
        sprite1.position = ccp(1130, 458);
        [self addChild:sprite1];
        
        
        CCSprite *sprite4 = [CCSprite spriteWithFile:@"kira15_atama.pvr.gz"];
        sprite4.anchorPoint = CGPointMake(0.2f, 0.10f);
        sprite4.position = ccp(235, -150);
        [self addChild:sprite4];
        
        CCSprite *sprite5 = [CCSprite spriteWithFile:@"kira15_dou2.pvr.gz"];
        sprite5.position = ccp(265, -200);
        [self addChild:sprite5];
        
        
        id move1 = [CCMoveTo actionWithDuration: 1.0f position:ccp(675, 515)];
        id ease1= [CCEaseSineOut actionWithAction:move1];
        [sprite3 runAction:ease1];
        
        id move2 = [CCMoveTo actionWithDuration: 1.0f position:ccp(716, 398)];
        id ease2= [CCEaseSineOut actionWithAction:move2];
        [sprite1 runAction:ease2];
        
        id move3 = [CCMoveTo actionWithDuration: 0.8f position:ccp(335, 360)];
        id ease3= [CCEaseSineOut actionWithAction:move3];
        id action4 = [CCSequence actions:[CCDelayTime actionWithDuration:0.5f],ease3,nil]; 
        [sprite4 runAction:action4];
        
        id move4 = [CCMoveTo actionWithDuration: 0.8f position:ccp(365, 212)];
        id ease4= [CCEaseSineOut actionWithAction:move4];
        id action5 = [CCSequence actions:[CCDelayTime actionWithDuration:0.55f],ease4,nil]; 
        [sprite5 runAction:action5];
        
        id move5 = [CCMoveTo actionWithDuration: 1.0f position:ccp(850, 512)];
        id ease5= [CCEaseSineOut actionWithAction:move5];
        id action6 = [CCSequence actions:[CCDelayTime actionWithDuration:1.5f],ease5,nil]; 
        [sprite6 runAction:action6];
        
        id move6 = [CCMoveTo actionWithDuration: 0.8f position:ccp(300, 500)];
        id ease7= [CCEaseSineOut actionWithAction:move6];
        id action7 = [CCSequence actions:[CCDelayTime actionWithDuration:0.5f],ease7,nil]; 
        [sprite7 runAction:action7];
        
        id Rote = [CCRotateBy actionWithDuration: 1.0f angle:-20]; 
        id Rote2 = [CCRotateBy actionWithDuration: 1.0f angle:20]; 
        id ease6= [CCEaseElasticOut actionWithAction:Rote2];
        id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],Rote,ease6,nil]; 
        id rep = [CCRepeatForever actionWithAction: [[action1 copy] autorelease]];
        [sprite3 runAction:rep];
        
        id Rote3 = [CCRotateBy actionWithDuration: 1.0f angle:10]; 
        id Rote4 = [CCRotateBy actionWithDuration: 1.0f angle:-10]; 
        id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],Rote3,Rote4,nil]; 
        id rep2 = [CCRepeatForever actionWithAction: [[action2 copy] autorelease]];
        [sprite4 runAction:rep2];        
        
        
    }else if(page == 16){
        //BGM読み込み
        [SoundPlayer bgmStop:optionnum3[1]:1];
        [SoundPlayer bgmPlay:optionnum3[1]:0];
        
        sprite2[0] = [CCSprite spriteWithFile:@"kira16_ude.pvr.gz"];
        sprite2[0].anchorPoint = CGPointMake(1.0f, 0.8f);
        sprite2[0].position = ccp(900, 0);
        [self addChild:sprite2[0]];
        
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira16_dou.pvr.gz"];
        sprite3.position = ccp(1024, 0);
        [self addChild:sprite3];
        
        id Move = [CCMoveBy actionWithDuration: 1.0f position:ccp(660-900, 375)]; 
        id Move2 = [CCMoveBy actionWithDuration: 1.0f position:ccp(790-1024, 443*0.5f)];
        id ease1= [CCEaseSineOut actionWithAction:Move];
        id ease2= [CCEaseSineOut actionWithAction:Move2];
        
        
		id fanc = [CCCallFunc actionWithTarget:self selector:@selector(movescene)];
        
        id action2 = [CCSequence actions:ease1,fanc,nil];    
        [sprite2[0] runAction:action2];
        [sprite3 runAction:ease2];
        
    }else if(page == 17){
        CGPoint location = CGPointMake(580, 400);
        CGPoint location2 = CGPointMake(418, 420);
        NSString *name = [[NSString alloc] initWithString:@"kira17_kane"];
        NSString *name2 = [[NSString alloc] initWithString:@"kira17_kane2"];
        
        [self  addNewSpriteWithJoint3:location:location2:name:name2];
        
        CCSprite *sprite1 = [CCSprite spriteWithFile:@"kira17_hashira.pvr.gz"];
        sprite1.position = ccp(370, 451);
        [self addChild:sprite1];
        
        CCSprite *sprite3 = [CCSprite spriteWithFile:@"kira17_inori.pvr.gz"];
        sprite3.position = ccp(810, 250);
        [self addChild:sprite3];
        
    }else if(page == 18){
        CCSprite *sprite1 = [CCSprite spriteWithFile:@"kira18_kouzuke.pvr.gz"];
        sprite1.position = ccp(550, -sprite1.contentSize.height/2);
        [self addChild:sprite1];
        
        id Move = [CCMoveBy actionWithDuration: 1.0f position:ccp(0, 310+sprite1.contentSize.height/2)]; 
        id ease1= [CCEaseSineOut actionWithAction:Move];
        id Sec = [CCSequence actions:ease1, nil];
        [sprite1 runAction:Sec];
    }
    
    
}

-(void)sece2{
    CGPoint location = CGPointMake(720, 460);
    CGPoint location2 = CGPointMake(300, 330);
    NSString *name = [[NSString alloc] initWithString:@"kira02_akou"];
    NSString *name2 = [[NSString alloc] initWithString:@"kira02_kouzukenosuke"];
    
    [self addNewSpriteWithJoint2:location:location2:name:name2];
    
    [self removeChild:sprite2[0] cleanup:YES];
    [self removeChild:sprite2[1] cleanup:YES];
    
}
-(void)sece6{
    if(optionnum3[1] == 0){
        se = [[SimpleAudioEngine sharedEngine] playEffect:@"horse.caf"];
    }
    CGPoint location = CGPointMake(550, 450);
    CCSprite *sprite6 = [CCSprite spriteWithFile:@"kira_uma_shita.pvr.gz"];
    sprite6.position = ccp(location.x, location.y-171);
    [self addChild:sprite6];
    NSString *name = [[NSString alloc] initWithString:@"kira6_uma_ue"];
    [self  addNewSpriteWithJoint:location:name];
    [self removeChild:sprite2[0] cleanup:YES];
}

-(void)sece12{
    CGPoint location = CGPointMake(554, 460);
    
    CCSprite *sprite6 = [CCSprite spriteWithFile:@"kira12_dou.pvr.gz"];
    sprite6.position = ccp(location.x+1, location.y-240);
    [self addChild:sprite6];
    
    NSString *name = [[NSString alloc] initWithString:@"kira12_head"];
    [self  addNewSpriteWithJoint:location:name];
    [self removeChild:sprite2[0] cleanup:YES];
}
-(void)wave{
    id Move1 = [CCMoveTo actionWithDuration: 1.0f position:ccp(595,580)];
    id ease1 = [CCEaseSineOut actionWithAction:Move1];
    id Move2 = [CCMoveTo actionWithDuration: 1.0f position:ccp(595,570)];
    id ease2 = [CCEaseSineIn actionWithAction:Move2];
    
    id Sec = [CCSequence actions:ease1,ease2, nil];
    id rep = [CCRepeatForever actionWithAction: [[Sec copy] autorelease]];
    [sprite2[0] runAction:rep];
}
-(void)wave2{
    id Move1 = [CCMoveTo actionWithDuration: 1.3f position:ccp(520,480)];
    id ease1 = [CCEaseSineOut actionWithAction:Move1];
    id Move2 = [CCMoveTo actionWithDuration: 1.3f position:ccp(520,500)];
    id ease2 = [CCEaseSineIn actionWithAction:Move2];
    
    id Sec = [CCSequence actions:ease1,ease2, nil];
    id rep = [CCRepeatForever actionWithAction: [[Sec copy] autorelease]];
    [sprite2[1] runAction:rep];
}

-(void)wave3{
    id Move1 = [CCMoveTo actionWithDuration: 1.7f position:ccp(390,370)];
    id ease1 = [CCEaseSineOut actionWithAction:Move1];
    id Move2 = [CCMoveTo actionWithDuration: 1.7f position:ccp(390,350)];
    id ease2 = [CCEaseSineIn actionWithAction:Move2];
    
    id Sec = [CCSequence actions:ease1,ease2, nil];
    id rep = [CCRepeatForever actionWithAction: [[Sec copy] autorelease]];
    [sprite2[2] runAction:rep];
}

-(void)wave4{
    id Move1 = [CCMoveTo actionWithDuration: 2.0f position:ccp(300,210)];
    id ease1 = [CCEaseSineOut actionWithAction:Move1];
    id Move2 = [CCMoveTo actionWithDuration: 2.0f position:ccp(300,230)];
    id ease2 = [CCEaseSineIn actionWithAction:Move2];
    
    id Sec = [CCSequence actions:ease1,ease2, nil];
    id rep = [CCRepeatForever actionWithAction: [[Sec copy] autorelease]];
    [sprite2[3] runAction:rep];
}

-(void)movescene{
    id Rote = [CCRotateBy actionWithDuration: 1.0f angle:-20]; 
    id Rote2 = [CCRotateBy actionWithDuration: 1.0f angle:20]; 
    id ease3= [CCEaseElasticOut actionWithAction:Rote2];
    id action1 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],Rote,ease3,nil]; 
    id rep = [CCRepeatForever actionWithAction: [[action1 copy] autorelease]];
    [sprite2[0] runAction:rep];
    
    CCMenuItemImage *taikoMenu = [CCMenuItemImage 
                                  itemWithNormalImage:@"kira16_taiko.png" 
                                  selectedImage: @"kira16_taiko2.png"
                                  target:self
                                  selector:@selector(onMusicSound:)];
    taikoMenu.position = ccp(93, -193);
    taikoMenu.tag =1;
    
    CCMenu * menu = [CCMenu menuWithItems:taikoMenu, nil];
    [self addChild:menu];
    
    
}

//シーン4の
-(void)scene4{
    id move1 = [CCMoveTo actionWithDuration:2.0f position:ccp(-50, 20)];
    id ease = [CCEaseSineIn actionWithAction:move1];
    id move2 = [CCMoveTo actionWithDuration:2.0f position:ccp(1000, 0)];
    id ease2 = [CCEaseSineIn actionWithAction:move2];
    id action = [CCSequence actions:[CCDelayTime actionWithDuration:1.0],ease,nil];
    id action2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0],ease2,nil];
    
    id fade = [CCFadeOut actionWithDuration:1.5f];
    id action3 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0],fade,nil];
   
    [Movie_Sprite[0] runAction:action3];
    [Movie_Sprite[1] runAction:action];
    [Movie_Sprite[2] runAction:action2];
}

-(void) draw
{
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();
	
	kmGLPopMatrix();
    
}





-(void) tick: (ccTime) dt
{  
    //鐘衝突音
    if(page == 17){
        CGRect projectileRect = CGRectMake(
                                           sprite2[1].position.x -(sprite2[1].contentSize.width/2), 
                                           sprite2[1].position.y -(sprite2[1].contentSize.height/2), 
                                           sprite2[1].contentSize.width, 
                                           sprite2[1].contentSize.height);
        CGRect targetRect = CGRectMake(
                                       sprite2[0].position.x -(sprite2[0].contentSize.width/2), 
                                       sprite2[0].position.y -(sprite2[0].contentSize.height/2), 
                                       sprite2[0].contentSize.width, 
                                       sprite2[0].contentSize.height);
        
        if(CGRectIntersectsRect(projectileRect, targetRect) && kane){
            kanecount++;
            if(kanecount <= 1){
                
                if(optionnum3[1] == 0){
                    //効果音
                    se = [[SimpleAudioEngine sharedEngine] playEffect:@"kane.caf"];
                }
                
            }
        } 
    }
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
	
	//Iterate over the bodies in the physics world
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			CCSprite *myActor = (CCSprite*)b->GetUserData();
			myActor.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
			myActor.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
		}	
	}
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if(touche){
        if(page == 6 || page == 12 || page == 4 || page == 2 || page==17){
            if (_mouseJoint != NULL) return;
            UITouch *myTouch =[touches anyObject];
            CGPoint location  = [myTouch locationInView:[myTouch view]];
            kaneTouch = [myTouch locationInView:[myTouch view]];
            location = [[CCDirector sharedDirector] convertToGL:location];
            b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
            
            
                if (page == 2) {
                    for (int i=0; i<2; i++) {
                        if (_paddleFixture2[i] -> TestPoint(locationWorld)) {
                            b2MouseJointDef md;
                            md.bodyA = groundBody;
                            md.bodyB = m_bodies[i];
                            md.target = locationWorld;
                            md.collideConnected = false;
                            md.maxForce = 120000.0f * m_bodies[i]->GetMass();
                            _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                            m_bodies[i]->SetAwake(true);
                        }
                    }
                }else{
                    b2MouseJointDef md;
                    md.bodyA = groundBody;
                    md.bodyB = body;
                    md.target = locationWorld;
                    md.collideConnected = false;
                    if(page == 4){
                        if (_paddleFixture -> TestPoint(locationWorld)) {
                            md.maxForce = 120000.0f * body->GetMass();
                        }
                    }else if(page == 6){
                        if (_paddleFixture -> TestPoint(locationWorld)) {
                            md.maxForce = 500.0f * body->GetMass();
                        }
                    }else{
                        md.maxForce = 1000.0f * body->GetMass();
                    }
                    
                    _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                    body->SetAwake(true);
                }
        }else if(touche && page == 13){
            if(ishitouch < 70){
                if(optionnum3[1] == 0){
                    [SoundPlayer PlaySE:optionnum3[1]:7];
                }
                ishitouch++;
                srand((unsigned)time(NULL));
                int kekka = random() % 4;
                UITouch *myTouch = [touches anyObject];
                CGPoint location = [myTouch locationInView:[myTouch view]];
                location = [[CCDirector sharedDirector] convertToGL:location];
                if (kekka == 0) {
                    NSString *name = [[NSString alloc] initWithString:@"kira13_ishi1"];
                    [self addNewSpriteWithCoords:location:name];
                }else if(kekka == 1){
                    NSString *name = [[NSString alloc] initWithString:@"kira13_ishi2"];
                    [self addNewSpriteWithCoords:location:name];
                }else if(kekka == 2){
                    NSString *name = [[NSString alloc] initWithString:@"kira13_ishi3"];
                    [self addNewSpriteWithCoords:location:name];
                }else if(kekka == 3){
                    NSString *name = [[NSString alloc] initWithString:@"kira13_ishi4"];
                    [self addNewSpriteWithCoords:location:name];
                }
            }else{
                touche = false;
                [self kansei];
            }
            
        }
    }
}
-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    if(page ==17){
        kanecount =0;
        float kaneTouch_x =kaneTouch.x;
        float loc_x =location.x;
        if(loc_x < kaneTouch_x){
            kane = false;
        }else{
            kane = true;
        }
    }
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    _mouseJoint->SetTarget(locationWorld);
    
}
-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}


- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    
    if (_mouseJoint) {
        int loc_x = location.x-kaneTouch.x;
        int loc_y = location.y-kaneTouch.y;
        
        
        BOOL Move =false;
        if(loc_x >= 50 || loc_y >= 50 || loc_x<-50 || loc_y<-50){
            Move = true;
        }
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
        if(optionnum3[1] == 0){
            if(page == 6 && Move){
                [SoundPlayer PlaySE:optionnum3[1]:2];
            }else if(page == 12 && Move){
                [SoundPlayer PlaySE:optionnum3[1]:6];
            }
        }
    }  
}

-(void)kansei{
    [SoundPlayer PlaySE:optionnum3[1]:8];
    CCSprite *sprite3 = [CCSprite spriteWithFile:@"kansei.pvr.gz"];
    sprite3.position = ccp(512, 368);
    sprite3.opacity =0.0f;
    
    [self addChild:sprite3];
    
    id move1 = [CCFadeIn actionWithDuration:1.0f];
    [sprite3 runAction:move1];
    
    
}

//パーティクルのpreload
-(void)preloadParticleEffect:(NSString *)particleFile{
    [CCParticleSystemQuad particleWithFile:particleFile];
}
- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{	
    static float prevX=0, prevY=0;
    
    //#define kFilterFactor 0.05f
#define kFilterFactor 1.0f	// don't use filter. the code is here just as an example
    
    float accelX = (float) acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
    float accelY = (float) acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
    
    prevX = accelX;
    prevY = accelY;
    
    // accelerometer values are in "Portrait" mode. Change them to Landscape left
    // multiply the gravity by 10
    b2Vec2 gravity( -accelY * 10, accelX * 10);
    
    world->SetGravity( gravity );
}

//設定用のファイルパス-------------------------------------------------------------------
- (NSString *)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"settei.plist"];
}

//設定ファイル読み込み
-(void)setteiRead{
    NSString *filePath = [self dataFilePath];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (int i = 0; i<3; i++) {
            NSString *Settei  = [[NSString alloc] init];
            Settei = [array objectAtIndex:i];
            optionnum3[i] = [Settei intValue];
        }
        [array release];
    }
    
}


//太鼓
-(void)soundEffect{
    
    taikoMenu1 = [CCMenuItemImage 
                  itemWithNormalImage:@"kira07_left_taiko.png" 
                  selectedImage: @"kira07_left_taiko2.png"
                  target:self
                  selector:@selector(onMusicSound:)];
    taikoMenu1.position = ccp(-143, 138);
    taikoMenu1.tag =1;
    
    taikoMenu2 = [CCMenuItemImage 
                  itemWithNormalImage:@"kira07_center_taiko.png" 
                  selectedImage: @"kira07_center_taiko2.png"
                  target:self
                  selector:@selector(onMusicSound:)];
    taikoMenu2.position = ccp(16.5, 292.5);
    taikoMenu2.tag =2;
    
    taikoMenu3 = [CCMenuItemImage 
                  itemWithNormalImage:@"kira07_right_taiko.png" 
                  selectedImage: @"kira07_right_taiko2.png"
                  target:self
                  selector:@selector(onMusicSound:)];
    taikoMenu3.position = ccp(176, 139);
    taikoMenu3.tag =3;
    
    CCMenu * menu2 = [CCMenu menuWithItems:taikoMenu1, taikoMenu2, taikoMenu3, nil];
    [self addChild:menu2 z:2];
}
-(void)onMusicSound:(id)sender{
    if([sender tag] == 1){
        [SoundPlayer PlaySE:optionnum3[1]:3];
    }else if([sender tag] == 2){
        [SoundPlayer PlaySE:optionnum3[1]:4];
    }else if([sender tag] == 3){
        [SoundPlayer PlaySE:optionnum3[1]:5];
    }else if([sender tag] == 4){
        if(touche){
            touche = false;
            [SoundPlayer PlaySE:optionnum3[1]:1];
            id jumpBy = [CCJumpBy actionWithDuration:0.4f position:ccp(0,0) height:300 jumps:1];
            id fanc1 = [CCCallFunc actionWithTarget:self selector:@selector(check)];
            id action =[CCSequence actions:jumpBy,fanc1,nil];
            [sprite2[0] runAction:action]; 
        }
    }
}

-(void)check{
    touche =true; 
}


- (void) dealloc
{
    // in case you have something to dealloc, do it in this method
    delete world;
    world = NULL;
    
    delete m_debugDraw;
    
    // don't forget to call "super dealloc"
    [super dealloc];
}
@end
