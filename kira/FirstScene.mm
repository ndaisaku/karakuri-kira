#import "FirstScene.h"
#import "ScenesManager.h"

@implementation FirstScene
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage {
    CCScene *scene = [FirstScene node];
    CCLayer *layer = [FirstLayer layerWithSceneId:sceneId];
    
    [scene addChild:layer];
    return scene;
}
@end

#pragma mark -
//FirstLayerクラス
@implementation FirstLayer
#pragma mark インスタンスの初期化/解放

// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId{
    return [[[self alloc] initWithSceneId:sceneId] autorelease];
}

-(id)initWithSceneId:(NSInteger)sceneId{
    self = [super init];
    if(self){
        self.tag = sceneId;
        self.isTouchEnabled = NO;
        
        //画面サイズ
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        if(sceneId == 0){
            [[SimpleAudioEngine sharedEngine] preloadEffect:@"title.caf"];
            CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"setumei1.pvr.gz"];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack];
            //設定の保存
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (int i=0; i<3; i++) {
                if(i == 2){
                    [array addObject:[NSNumber numberWithInt:1]];
                }else {
                    [array addObject:[NSNumber numberWithInt:0]];
                }
                [array writeToFile:[self dataFilePath] atomically:YES];
            }
            [array release];
            
            
        }else if(sceneId == 1){
            CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"setumei2.pvr.gz"];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack];
        }
        else if(sceneId == 2){
            self.isTouchEnabled = YES;
            
            
            
            CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"kira0.pvr.gz"];
            CCSprite *spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
            spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
            [self addChild: spriteBack];
            
            CCSprite *sprite2 = [CCSprite spriteWithFile:@"title-text.pvr.gz"];
            sprite2.position = ccp(200+sprite2.contentSize.width/2, 450);
            [self addChild:sprite2];
            
            id fanc = [CCCallFunc actionWithTarget:self selector:@selector(Storystart)];
            id action1 = [CCSequence actions:fanc,nil];
            [sprite2 runAction:action1];
                       
            
            CCMenuItemImage *systemmenu = [CCMenuItemImage 
                                           itemWithNormalImage:@"system.pvr.gz" 
                                           selectedImage: @"system.pvr.gz"
                                           target:self
                                           selector:@selector(choosedA:)];
            systemmenu.position = ccp(-450, 330);
            systemmenu.tag = 1;
            CCMenu *menu = [CCMenu menuWithItems:systemmenu, nil];
            [self addChild:menu];
            [self Storystart];
        }
        
        if(sceneId < 2){
            
            CCMenuItemImage *nextBTN = [CCMenuItemImage 
                                        itemWithNormalImage:@"title_next.pvr.gz" 
                                        selectedImage: @"title_next.pvr.gz"
                                        target:self
                                        selector:@selector(choosedA:)];
            nextBTN.position = ccp(0, -330);
            nextBTN.tag = 2;
            CCMenu *menu = [CCMenu menuWithItems:nextBTN, nil];
            [self addChild:menu];
        }
        
        
    }
    return self;
}
-(void)Storystart{
    [self setteiRead];
    CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"start.pvr.gz"];
    CCSprite *sprite3 = [CCSprite spriteWithTexture:background];
    sprite3.opacity =0.0f;
    sprite3.position = ccp(512, 100);
    [self addChild:sprite3]; 
    
    id Fade = [CCFadeIn actionWithDuration:1.0];
    id Fade2 = [CCFadeOut actionWithDuration:1.0];
    id action1 = [CCSequence actions:Fade,[CCDelayTime actionWithDuration:2.0f],Fade2,nil];
    id rep = [CCRepeatForever actionWithAction: [[action1 copy] autorelease]];
    [sprite3 runAction:rep];
    start = true;
    
    //タイトル音再生
    if(optionnum3[0] == 0){
        CDLongAudioSource *player = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
        [player load:@"title.caf"];
        player.volume = 1.0f;
        [player setNumberOfLoops:0];
        [player play];
    }
}


#pragma mark タイトルイベント
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if(start){
        [self choosedA:nil];
    }
}

#pragma mark イベント処理
- (void)choosedA:(id)sender {
    if([sender tag] == 1){
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseD next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
        [self FadeOut];
        
    }else if([sender tag] == 2){
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseA next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
        
    }else{
        CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseA next:true];
        [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
        [self FadeOut];

    }
}

-(void)FadeOut{
    CDLongAudioSource *player = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
    CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player interpolationType:kIT_Exponential startVal:player.volume endVal:0.0];
    [fader setStopTargetWhenComplete:YES];
    //Create a property modifier action to wrap the fader
    CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:1.0 modifier:fader];
    [fader release];//Action will retain
    //id actionCallFuncN = [CCCallFuncN actionWithTarget:self selector:@selector(actionComplete:)];
    [[CCDirector sharedDirector].actionManager addAction:[CCSequence actions:action, nil] target:player paused:NO];
}

//設定用のファイルパス-------------------------------------------------------------------
- (NSString *)dataFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return [documentsDirectory stringByAppendingPathComponent:@"settei.plist"];
}

//設定ファイル読み込み
-(void)setteiRead{
    NSString *filePath = [self dataFilePath];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (int i = 0; i<3; i++) {
            NSString *Settei  = [[NSString alloc] init];
            Settei = [array objectAtIndex:i];
            optionnum3[i] = [Settei intValue];
        }
        [array release];
    }
    
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [[CCTextureCache sharedTextureCache] removeAllTextures];
	[super dealloc];
}
@end
