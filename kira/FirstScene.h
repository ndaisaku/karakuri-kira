#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTransitionHelper.h"
#import "SimpleAudioEngine.h"
#import "CDXPropertyModifierAction.h"

@interface FirstScene : CCScene {
}
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;

@end

@interface FirstLayer : CCLayer{
    bool start;
    ALint titleVoice;
    int optionnum3[3];
}

// 初期化処理を行った上でインスタンスを返すメソッド
+ (id)layerWithSceneId:(NSInteger)sceneId;
- (id) initWithSceneId:(NSInteger)sceneId;

// タップされたときのイベントを記述するメソッド
- (void)choosedA:(id)sender;
- (NSString *)dataFilePath;
-(void)Storystart;
-(void)FadeOut;
-(void)setteiRead;
@end


