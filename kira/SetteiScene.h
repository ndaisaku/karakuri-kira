#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTransitionHelper.h"
#import <UIKit/UIWebView.h>

@interface SetteiScene : CCScene {
}
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;

@end

@interface SetteiLayer : CCLayer <UIWebViewDelegate>{
    bool start;
    CCSprite *sprite2[3];
    int optionnum3[3];
    UIWebView *webView_;
	UIToolbar *toolBar;
    UIActivityIndicatorView *activityInducator_;
    
}

// 初期化処理を行った上でインスタンスを返すメソッド
+ (id)layerWithSceneId:(NSInteger)sceneId;
- (id) initWithSceneId:(NSInteger)sceneId;

// タップされたときのイベントを記述するメソッド

-(NSString *)dataFilePath;
-(void)setteiRead;
-(void)pressMenuItem:(id)sender;
-(void)pressMenuItem2:(id)sender;

@end
