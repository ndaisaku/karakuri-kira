

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

#define kSceneChoiseA   0
#define kSceneChoiseB   1
#define kSceneChoiseC   2
#define kSceneChoiseD   3
#define kSceneChoiseE   4

@interface SceneManager : NSObject {
    BOOL naration;
}


+ (CCScene *)nextSceneOfScene:(CCNode *)from choice:(NSInteger)choise next:(BOOL)nextPage;
+ (NSString *)textForSceneId:(int)sceneId;
@end