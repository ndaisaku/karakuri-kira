#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "CCTransitionHelper.h"
#import "CDXPropertyModifierAction.h"
#import "SimpleAudioEngine.h"
#import "SoundPlayer.h"

// HelloWorldLayer
@interface MiddleScene : CCScene
{
	
}

// MiddleLayerを追加済みのシーンを返す
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;

@end

// MiddleLayerクラス
// MiddleSceneクラス上に表示するレイヤー。このレイヤー上に
// 背景やテキスト、選択肢を配置します。
@interface MiddleLayer : CCLayer {
    b2World* world;
	GLESDebugDraw *m_debugDraw;
    CCTexture2D *texture[19];
    int optionnum3[3];
    CCMenuItemImage *maryItem3;
    CCMenuItemImage *maryItem4;
    CCSprite *textBG_Sprite;
    int page;
    b2Body* m_bodies[2];
	b2Joint* m_joints[12];
    b2Fixture *_bottomFixture;
    CCSprite *Movie_Sprite[15];
    b2Fixture *_paddleFixture;
    b2Fixture *_paddleFixture2[2];
    CCSprite* sprite2[20];

    
    
    b2MouseJoint *_mouseJoint;
    b2Body *body;
    b2Body* groundBody;
    
    b2RevoluteJoint* m_joint1;
	b2PrismaticJoint* m_joint2;
    
    BOOL touche;
    

    
    CCMenu *scene2Menu;
    bool next;
    BOOL kane;
    int kanecount;
    
    CDLongAudioSource *player;
    
    int page15Count;
    
    
    int iaRandarray[15]; /*乱数候補収納用変数*/
    int iCounter; /*ループカウンタ用変数*/
    int iNumRand; /*残り乱数候補数*/
    int iRandKey; /*乱数候補取得用変数*/
    int iRandVal; /*乱数の取得用変数*/
    
    int setteino[3];
    ALuint se;
    CCSprite *spriteBack;
    CGPoint kaneTouch;
    int ishitouch;
    
    CCMenuItemImage *taikoMenu1;
    CCMenuItemImage *taikoMenu2;
    CCMenuItemImage *taikoMenu3;
    
@private
}

// 初期化処理を行った上でインスタンスを返すメソッド
+ (id)layerWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;
- (id)initWithSceneId:(NSInteger)sceneId:(BOOL)nextPage;
-(void) addNewSpriteWithCoords:(CGPoint)p:(NSString *)name;
-(void) addNewSpriteWithJoint:(CGPoint)p:(NSString *)name;
//-(void)addNewSpriteWithJoint;
-(void)PageEffect;
-(void)Playnarration;
-(void)setteiRead;
-(void)scene4;
-(void)FadeOut;
@end
