//
//  SoundPlayer.m
//  kira
//
//  Created by mac on 11/08/31.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SoundPlayer.h"

SystemSoundID soundID[10];
ALuint narr;
ALuint se;
NSInteger sound_count;
NSInteger sound_count2;
//SimpleAudioEngine *narration;
AVAudioPlayer *narrtion[23];
AVAudioPlayer *bgm[5];

@implementation SoundPlayer
+(void)sePlay:(NSInteger)switchNo:(NSInteger)Page{
    if(switchNo == 0){
        [[SimpleAudioEngine sharedEngine]  preloadEffect:@"uma.caf"];
        [[SimpleAudioEngine sharedEngine]  preloadEffect:@"gensou.caf"];
        [[SimpleAudioEngine sharedEngine]  preloadEffect:@"wasse.caf"];
        [[SimpleAudioEngine sharedEngine]  preloadEffect:@"kansei.caf"];
        [self seLoad:@"kirareru":0];
        [self seLoad:@"sei_ka_koban02":1];
        [self seLoad:@"horse":2];
        [self seLoad:@"taiko":3];
        [self seLoad:@"taiko2":4];
        [self seLoad:@"taiko3":5];
        [self seLoad:@"byon":6];
        [self seLoad:@"ishi":7];
        [self seLoad:@"kansei2":8];
        [self seLoad:@"kane":9];
        
    }
}

+(void)bgmLoad:(NSInteger)switchNo{
    if (switchNo == 0) {
        [self bgmLoad2:@"bgm1":0];
        [self bgmLoad2:@"bgm2":1];
        [self bgmLoad2:@"bgm3":2];
        [self bgmLoad2:@"kaminari":3];
        [self bgmLoad2:@"mokugyo":4];
    }
}

+(void)bgmLoad2:(NSString *)Path:(NSInteger)num{
    
    NSString* a_file_path = [[NSBundle mainBundle] pathForResource:Path ofType:@"caf"];
    NSURL *url = [NSURL fileURLWithPath:a_file_path];
    bgm[num] = [[AVAudioPlayer alloc] initWithContentsOfURL:url 
                                                      error:NULL];
}

+(void)bgmPlay:(NSInteger)switchNo:(NSInteger)num{
    if (switchNo == 0) {
        sound_count = num;
        bgm[num].numberOfLoops = -1;
        if(num == 3){
            bgm[num].volume = 1.0f;
        }else{
            bgm[num].volume = 0.6f;
        }
        bgm[num].currentTime = 0;
        [bgm[num] play];
    }
}

+(void)bgmStop:(NSInteger)switchNo:(NSInteger)num{
    if (switchNo == 0) {
        [bgm[num] stop];
    }
}


+(void)bgmStop2:(NSInteger)switchNo{
    if (switchNo == 0) {
        
        [bgm[sound_count] stop];
        [bgm[sound_count] release];
    }
}

+(void)narraLoad:(NSInteger)switchNo:(NSInteger)Page3{
    if (switchNo == 0) {
        NSString *narrapath  =[[NSString alloc] init];
        narrapath  = [NSString stringWithFormat:@"scene%d",Page3];
        
        NSString* a_file_path = [[NSBundle mainBundle] pathForResource:narrapath ofType:@"caf"];
        NSURL *url = [NSURL fileURLWithPath:a_file_path];
        narrtion[Page3] = [[AVAudioPlayer alloc] initWithContentsOfURL:url 
                                                                 error:NULL];
        
    }
    
}

+(void)seLoad:(NSString *)path:(NSInteger)num{
    NSString* a_file_path = [[NSBundle mainBundle] pathForResource:path ofType:@"caf"];
    NSURL *url = [NSURL fileURLWithPath:a_file_path];
    AudioServicesCreateSystemSoundID((CFURLRef)url, &soundID[num]);
}

+(void)PlaySE:(NSInteger)switchNo:(NSInteger)num{
    if(switchNo == 0){
        AudioServicesPlaySystemSound(soundID[num]);
    }
}
+(void)StopSE:(NSInteger)num{
    for (int i= 0; i<4; i++) {
        AudioServicesDisposeSystemSoundID(soundID[i]);
        
    }
}



+(void)PlaySE2:(NSInteger)switchNo:(NSString *)path{
    if(switchNo == 0){
        se = [[SimpleAudioEngine sharedEngine] playEffect:path];
    }
}



+(void)PlayBG:(NSInteger)switchNo:(NSString *)path{
    if(switchNo == 0){
        [SimpleAudioEngine sharedEngine].backgroundMusicVolume = 0.6;
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:path];
    }
}

+(void)StopBG:(NSInteger)switchNo{
    if(switchNo == 0){
        [SimpleAudioEngine sharedEngine].backgroundMusicVolume = 0.6;
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    }
}



+(void)PlayNarration:(NSInteger)Page{
    [narrtion[Page] play];
}

+(void)StopNarr:(NSInteger)switchNo:(NSInteger)Page{
    if(switchNo == 0){
        [narrtion[Page] stop];
        [narrtion[Page] release];
    }
}



+(void)StopSE2:(NSInteger)switchNo:(NSInteger)Page{
    CCLOG(@"PageEND%d",Page);
    [[SimpleAudioEngine sharedEngine] stopEffect:narr];
    [[SimpleAudioEngine sharedEngine] stopEffect:se];
    [SimpleAudioEngine sharedEngine].effectsVolume = 1.0;
}

+(void)countReset{
    sound_count = 0;
}



- (void) dealloc
{
	[super dealloc];
}

@end
