//
//  SoundPlayer.h
//  kira
//
//  Created by mac on 11/08/31.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioServices.h>
#import "SimpleAudioEngine.h"

@interface SoundPlayer : NSObject <AVAudioPlayerDelegate>{
}
+(void)sePlay:(NSInteger)switchNo:(NSInteger)Page;
+(void)narraLoad:(NSInteger)switchNo:(NSInteger)Page3;
+(void)PlaySE:(NSInteger)switchNo:(NSInteger)num;
+(void)StopSE:(NSInteger)num;
+(void)seLoad:(NSString *)path:(NSInteger)num;
+(void)PlayBG:(NSInteger)switchNo:(NSString *)path;
+(void)PlaySE2:(NSInteger)switchNo:(NSString *)path;
+(void)PlayNarration:(NSInteger)Page;
+(void)StopSE2:(NSInteger)switchNo:(NSInteger)Page;
+(void)StopNarr:(NSInteger)switchNo:(NSInteger)Page;
+(void)StopBG:(NSInteger)switchNo;
+(void)countReset;

+(void)bgmLoad:(NSInteger)switchNo;
+(void)bgmLoad2:(NSString *)Path:(NSInteger)num;
+(void)bgmPlay:(NSInteger)switchNo:(NSInteger)num;
+(void)bgmStop:(NSInteger)switchNo:(NSInteger)num;
+(void)bgmStop2:(NSInteger)switchNo;

@end
